clear all; clc;

if exist('settings','file')==2
    load settings
else 
    error('No setting data is detected!');
end
load input;

%% Solver options
input.opt.hessian='gauss_newton';  % 'gauss_newton', 'exact'
input.opt.qpsolver='qpoases'; %'ipopt','qpoases'
input.opt.condensing='standard';  %'standard','no','null-space'
input.opt.hotstart='no'; %'yes','no' (only for qpoases)
input.opt.shifting='yes'; % 'yes','no'

% IRK4 configurations
settings.Numstep=1; % No. of integration steps per interval for IRK4
settings.numIT=3; % No. of iterations for solving the nonlinear system in IRK4
settings.realtime=1; % 1,0 ; Flag to turn on/off a real-time trick for IRK4

%% Configuration (User defined)
addpath('nmpc');
addpath('Source_Codes');

if strcmp(input.opt.integrator, 'IRK4')
    addpath('IRK');
end

Ts  = settings.Ts;    
nx = settings.nx; 
nu = settings.nu;    
ny = settings.ny;     
nyN= settings.nyN;    
np = settings.np;    
nc = settings.nc;
ncN = settings.ncN;

nz = nx+nu;

N     = 40; % No. of shooting points
nw    = (N+1)*nx+N*nu; % No. of optimization varialbes
neq   = (N+1)*nx; % No. of equality constraints
nineq = N*2*nc+2*ncN; % No. of inequality constraints

settings.N  = N;
settings.nw = nw; 
settings.neq = neq;
settings.nineq = nineq; 

% globalization configurations (require advanced knowledge on globilization)
input.opt.meritfun.mu_merit=0;              % initialize the parameter
input.opt.meritfun.eta=1e-4;                % merit function parameter
input.opt.meritfun.tau=0.8;                 % step length damping factor
input.opt.meritfun.mu_safty=1.1;            % constraint weight update factor (for merit function)
input.opt.meritfun.rho=0.5;                 % merit function parameter
input.opt.meritfun.second_order_correction='no';  % trigger on second_order_correction

%% Initialize all solvers (solver dependent)

if strcmp(input.opt.qpsolver,'ipopt')
    ipopt_opts=ipoptset('constr_viol_tol',1e-3,'acceptable_tol',1e-3,'hessian_constant','yes',...
                        'mehrotra_algorithm','yes','mu_oracle','probing','jac_c_constant','yes',...
                        'jac_d_constant','yes','mu_strategy','adaptive','adaptive_mu_globalization',...
                        'never-monotone-mode','accept_every_trial_step','yes');
    if strcmp(input.opt.condensing,'no')
        input.opt.ipopt.options.eq=[false(nineq,1);true(neq,1)];
        input.opt.ipopt.options.ineq=[true(nineq,1);false(neq,1)];
        input.opt.ipopt.x0=zeros(nw,1);
    else
        input.opt.ipopt.options.eq=false(settings.nineq,1);
        input.opt.ipopt.options.ineq=true(settings.nineq,1);
        input.opt.ipopt.x0=zeros(N*nu,1);
    end
    input.opt.ipopt.options.nleq=[];
    input.opt.ipopt.options.nlineq=[];
    input.opt.ipopt.options.ipopt=ipopt_opts;
    input.opt.ipopt.options.ipopt.print_level=0;
end

if strcmp(input.opt.condensing,'standard') 
    M=zeros(nw,N*nu);
    M(1:nx+nu,1:nu)=[zeros(nx,nu);eye(nu,nu)];
    L=zeros(nw,nx);
    L(1:nx,:)=eye(nx);
    mG=zeros(nw,1);
    
    input.opt.condensing_matrix.M=M;   
    input.opt.condensing_matrix.L=L; 
    input.opt.condensing_matrix.mG=mG;   
end

if strcmp(input.opt.qpsolver,'qpoases')
    input.opt.condensing_matrix.QP=0;
end

if strcmp(input.opt.hessian,'exact') && strcmp(input.opt.integrator,'IRK4')
    warning('Exact Hessian using IRK4 integrator is now experimental');
    warning('The number of integration steps per interval for IRK4 is forced to ONE!');
    settings.Numstep=1;
end

if strcmp(input.opt.hessian,'exact')
    warning('Exact Hessian may be indefinite');
    cont=builtin('input','Continue the simulation using exact Hessian?(y/n)','s');
    if strcmp(cont,'n')
        error('Simulation terminated!');
    end
end

if strcmp(input.opt.qpsolver,'forces')
    if ~strcmp(input.opt.condensing,'no')      
        error('Primal Dual Interior Point method (Forces Pro) requires sparse formulation');
    end
end
    
%% Off-line data structure initialization

% I_dG
I_dG=zeros(neq,nw);
I_dG(1:nx,1:nx)=-eye(nx);
for i=1:N
    I_dG(i*nx+1:(i+1)*nx,i*(nx+nu)+1:i*(nx+nu)+nx)=-eye(nx);
end
I_dG=sparse(I_dG);

input.data.I_dG=I_dG;

% QP data

input.data.H=cell(1,N+1); 
input.data.g=zeros(nz,N);
input.data.G=zeros(nx,N);

if ~strcmp(input.opt.condensing,'standard')
    input.data.dG=cell(1,N);
else
    input.data.dG=zeros(nx,N*(nx+nu));
end

input.data.B=zeros(2*nc,N);
input.data.dB=cell(1,N+1);

% Multipliers

input.lambda=ones(nx,N+1);
input.mu=zeros(2*nc,N);
input.muN=zeros(2*ncN,1);

index=[];    % index for recovering multipliers when using condensing
for j=1:N
    index_j=(j-1)*(nx+nu)+1:(j-1)*(nx+nu)+nx;
    index=[index,index_j];
end
input.data.index_lam=[index,N*(nx+nu)+1:N*(nx+nu)+nx];

% others

input.data.obj_vec=zeros(ny,N);
input.data.dL=zeros(nz,N);

%% Initialzation

Initialization;
%% Simulation

iter = 1; time = 0.0;
Tf = 3;               % simulation time
state_sim= x0';
controls_MPC = u0';
y_sim = [];
constraints = [];
CPT = [];

mpc_callnum=1;         % maximum number of iterations for each sampling instant (for RTI, this is ONE)
kkt_lim=1e-2;          % tolerance on optimality

while time(end) < Tf
    
    % time-invariant reference
    Yref = REF;
    Yref = repmat(Yref,1,N);
    
    % time-varying reference, no preview
%     Yref = REF(:,iter);
%     Yref = repmat(Yref,1,N);

    % time-varying reference, preview
%     Yref = REF(:,iter:iter+N-1);
  
    input.y = Yref;    % ny by N matrix
    input.yN= Yref(1:nyN,end);  % nyN by 1 vector
       
    % obtain the state measurement
    input.x0 = state_sim(end,:)';
    
    % call the NMPC solver
    output=mpc_nmpcsolver(input,settings,mpc_callnum,kkt_lim);
    
    % obtain the solution and update the data
    switch input.opt.shifting
        case 'yes'
        input.z=[output.z(:,2:end),[output.xN; output.z(nx+1:nx+nu,end)]];  
        input.xN=output.xN;
        input.lambda=[output.lambda(:,2:end),output.lambda(:,end)];
        input.mu=[output.mu(:,2:end),[output.muN;output.mu(2*ncN+1:2*nc,end)]];
        input.muN=output.muN;
        case 'no'
        input.z=output.z;
        input.xN=output.xN;
        input.lambda=output.lambda;
        input.mu=output.mu;
        input.muN=output.muN;
    end
    input.opt.meritfun=output.meritfun;
    
    if strcmp(input.opt.qpsolver,'qpoases')
        input.opt.condensing_matrix.QP=output.opt.condensing_matrix.QP;
    end
    
    % collect the statistics
    cpt=output.info.cpuTime;
    tint=output.info.intTime;
    tsens=output.info.sensTime;
    tcond=output.info.condTime;
    tqp=output.info.qpTime;
    
    % Simulate system dynamics
    sim_input.x = state_sim(end,:).';
    sim_input.u = output.z(nx+1:nx+nu,1);
    sim_input.p = input.od(:,1);
    xf=full( Simulate_system('Simulate_system', sim_input.x, sim_input.u, sim_input.p) ); 
    
    % Collect outputs
    y_sim = [y_sim; full(h_fun('h_fun', xf, sim_input.u, sim_input.p))'];  
    
    % Collect constraints
    constraints=[constraints; full( ineq_fun('ineq_fun', xf, sim_input.u, sim_input.p) )'];
    
    % store the optimal solution and states
    controls_MPC = [controls_MPC; output.z(nx+1:nx+nu,1)'];
    state_sim = [state_sim; xf'];
    
    %
    CPT=[CPT;cpt, tint,tsens,tcond,tqp];
    
    % go to the next sampling instant
    nextTime = iter*Ts; 
    iter = iter+1;
    disp(['current time:' num2str(nextTime) '  CPT:' num2str(cpt) 'ms  INT:' num2str(tint) 'ms  SENS:' num2str(tsens) 'ms  COND:' num2str(tcond) 'ms  QP:' num2str(tqp) 'ms  KKT:' num2str(output.info.kktValue) '  numIT:' num2str(output.info.iteration_num)]);
    time = [time nextTime];
end

%% draw pictures (optional)
Draw;

%% clear the path and mex
clear mex; 
rmpath('nmpc');
rmpath('Source_Codes');
if strcmp(input.opt.integrator, 'IRK4')
    rmpath('IRK');
end