%% Initialization

switch settings.model
          
    case 'InvertedPendulum'
        x0 = [0;pi;0;0];    % initial state, nx by 1 vector
        u0 = zeros(nu,1);    % initial control, nu by 1 vector
        para0 = 0;  % initial parameters, ny by 1 vector (if ny=0, para0=0)
        
        Q=diag([10 10 0.1 0.1 0.01]);   % weights, ny by ny matrix
        QN=Q(1:nyN,1:nyN);   % terminal weights, nyN by nyN matrix

        lb=[-2;-20];   % lower bounds for constraints, nc by 1 vector
        ub=[2;20];     % upper bounds for constraints, nc by 1 vector
        lbN=-2;        % lower bounds for terminal constraints, ncN by 1 vector
        ubN=2;         % upper bounds for terminal constraints, ncN by 1 vector
        
        input.lb=repmat(lb,1,N);  % all lower bounds, nc by N matrix
        input.ub=repmat(ub,1,N);  % all upper bounds, nc by N matrix
        input.lb(:,1)=[-inf;-20];
        input.ub(:,1)=[inf;20];
        input.lbN=lbN;               % bounds of the first N stages
        input.ubN=ubN;               % bounds of the terminal stage
        
    case 'ChainofMasses_Lin'
        n=15;                        % number of masses
        x0=zeros(nx,1);
        for i=1:n
            x0(i)=7.5*i/n;
        end
        u0=zeros(nu,1);
        para0=0;
        wv=[];wx=[];wu=[];
        wu=blkdiag(wu,0.1, 0.1, 0.1);
        for i=1:3
            wx=blkdiag(wx,25);
            wv=blkdiag(wv,diag(0.25*ones(1,n-1),0));
        end
        Q=blkdiag(wx,wv,wu);
        QN=blkdiag(wx,wv);
        
        lb=[-1;-1;-1];
        ub=[1;1;1];
        lbN=-inf;
        ubN=inf;
        
        input.lb=repmat(lb,1,N);
        input.ub=repmat(ub,1,N); 
        input.lbN=lbN;               
        input.ubN=ubN; 
        
end

% prepare the data

x = repmat(x0,1,N+1);  % initialize all shooting points with the same initial state 
u = repmat(u0,1,N);    % initialize all controls with the same initial control
para = repmat(para0,1,N+1); % initialize all parameters with the same initial para

input.z=[x(:,1:N);u];        % states and controls of the first N stages, (nx+nu) by N matrix
input.xN=x(:,N+1);          % states of the terminal stage, nx by 1 vector
input.od=para;               % parameters, ny by N+1 matrix
input.W=Q;                   % weights of the first N stages, ny by ny matrix
input.WN=QN;                 % weights of the terminal stage, nyN by nyN matrix


%% Reference generation

    % REF should be a ny by 1 vector
switch settings.model
            
    case 'InvertedPendulum'
        
        REF=zeros(1,nx+nu)';   % referece, ny by 1 vector

    case 'ChainofMasses_Lin'
        
        REF=[7.5,0,0,zeros(1,3*(n-1)),zeros(1,nu)]';
        
end
   