function [ H, t_hess] = IRK_HESS_M(settings, sens_info, lambda_next )
    Ts=settings.Ts;
    Numstep=settings.Numstep;

    h = Ts/Numstep;   
    
    u0=sens_info.u;
    p=sens_info.p;
       
    J_phi_dk=sens_info.J_phi_dk;
    
    t_hess_start=tic;
    
    for j=1:Numstep             
                
        x0=sens_info.x(j,:)';
        k0=sens_info.k(j,:)';
        
        w0=[x0;u0];
               
        lambda_k=J_phi_dk'*lambda_next;
        J_k=full(dF_dk('jacobian_F_NS_0_0',k0,w0,p,h));
        lambda_k=mldivide(J_k',lambda_k);
        J_w=full(dF_dw('jacobian_F_NS_1_0',k0,w0,p,h));
        dk_dw=-mldivide(J_k,J_w);
        
        lambda_k_mat=full(d2F_d2k('d2F_d2k',k0,w0,p,h,lambda_k));
        MAT1=dk_dw'*lambda_k_mat*dk_dw;
                
        lambda_k_mat=full(d2F_dk_dw('jacobian_dF_dk_adj_1_0',k0,w0,p,h,lambda_k));       
        MAT2=dk_dw'*lambda_k_mat;
        
        lambda_k_mat=full(d2F_dw_dk('jacobian_dF_dw_adj_0_0',k0,w0,p,h,lambda_k));
        MAT3=lambda_k_mat*dk_dw;
        
        MAT4=full(d2F_d2w('d2F_d2w',k0,w0,p,h,lambda_k));
        
        H=-(MAT1+MAT2+MAT3+MAT4);
    end

    t_hess=toc(t_hess_start)*1e3;
end

