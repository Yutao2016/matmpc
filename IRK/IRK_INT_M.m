function [ x_end, tint, sens_info] = IRK_INT_M(settings,x0,u0,p )

    Numstep=settings.Numstep;
    numIT=settings.numIT;
    trick=settings.realtime;
    Ts=settings.Ts;

    h = Ts/Numstep;
    
    nx=settings.nx;
    s=3;
    k0=zeros(nx*s,1);
    J_phi_dk=full(dphi_dk('jacobian_phi_0_0',k0,x0,h));
    
    sens_info.k=zeros(Numstep,nx*s);
    sens_info.x=zeros(Numstep,nx);
    sens_info.u=u0;
    sens_info.p=p;
    sens_info.J_phi_dk=J_phi_dk;
    
    if trick
        opts1.LT=true;
        opts2.UT=true;
    end
    
    tint_start=tic;
    for j=1:Numstep      
        w0=[x0;u0]; 
        
        if trick
            J_k=full(dF_dk('jacobian_F_NS_0_0',k0,w0,p,h));
            [L,U,P]=lu(J_k);
        end
        
        for i=1:numIT
            
            if ~trick
                J_k=full(dF_dk('jacobian_F_NS_0_0',k0,w0,p,h)); 
            end
            
            b=-full(F_NS('F_NS',k0,w0,p,h));
            
            if trick
                y=linsolve(L,P*b,opts1);        
                dk=linsolve(U,y,opts2);
            else               
                dk=mldivide(J_k,b);
            end
            
            k0=k0+dk;   
        end
        x_end=full(phi('phi',k0,x0,h));     
        sens_info.x(j,:)=x0';
        sens_info.k(j,:)=k0';
        x0=x_end;             
    end
    tint=toc(tint_start)*1e3;
end

