function [ ADJ, tadj] = IRK_ADJ_SENS_M(settings, sens_info, lambda_next,lambdai )
    Ts=settings.Ts;
    Numstep=settings.Numstep;

    h = Ts/Numstep;   
    
    u0=sens_info.u;
    p=sens_info.p;
    
    nx=settings.nx;
    nu=settings.nu;
    
    lambda_next=[lambda_next;zeros(nu,1)];         
    
    dx_dw=[eye(nx),zeros(nx,nu)];
    du_dw=[zeros(nu,nx),eye(nu)];
    
    J_phi_dk=sens_info.J_phi_dk;
        
    tadj_start=tic;
    for j=Numstep:-1:1             
                
        x0=sens_info.x(j,:)';
        k0=sens_info.k(j,:)';
           
        dphi_dx_adj=lambda_next(1:nx,1);
        J_phi_dk_adj=J_phi_dk'*lambda_next(1:nx,1);
        
        w0=[x0;u0];
        J_k=full(dF_dk('jacobian_F_NS_0_0',k0,w0,p,h));
        dk_dw_adj=mldivide(J_k',J_phi_dk_adj);
        J_w_adj=full(dF_dw_adj('dF_dw_adj',k0,w0,p,h,dk_dw_adj));
        ADJj=dx_dw'*dphi_dx_adj-J_w_adj;
        
        ADJ=ADJj+du_dw'*lambda_next(nx+1:nx+nu,1);
        
        lambda_next=ADJ;
    end
    
    ADJ=ADJ-[lambdai;zeros(nu,1)];
    
    tadj=toc(tadj_start)*1e3;
end



