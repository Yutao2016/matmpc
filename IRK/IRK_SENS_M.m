function [ Sens, tsens] = IRK_SENS_M(settings, sens_info )
    Ts=settings.Ts;
    Numstep=settings.Numstep;

    h = Ts/Numstep;   
    
    u0=sens_info.u;
    p=sens_info.p;
    
    nx=settings.nx;
    nu=settings.nu;

    Sens=eye(nx+nu,nx+nu);
    
    Jdphi_dx=eye(nx,nx);
    dx_dw=[eye(nx),zeros(nx,nu)];
    du_dw=[zeros(nu,nx),eye(nu)];
    Sensj=[zeros(nx,nx+nu);du_dw];
   
    J_phi_dk=sens_info.J_phi_dk;
    
    tsens_start=tic;
    for j=1:Numstep             
                
        x0=sens_info.x(j,:)';
        k0=sens_info.k(j,:)';
        
        w0=[x0;u0];
        J_k=full(dF_dk('jacobian_F_NS_0_0',k0,w0,p,h));
        J_w=-full(dF_dw('jacobian_F_NS_1_0',k0,w0,p,h));
        dk_dw=mldivide(J_k,J_w);
        Sensj(1:nx,:)=Jdphi_dx*dx_dw+J_phi_dk*dk_dw;
        Sens=Sensj*Sens;
    end
    Sens=Sens(1:nx,:);
    tsens=toc(tsens_start)*1e3;
end

