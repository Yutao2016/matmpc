%% Plot results

blue=[0 0 255]/255;
red=[220 20 60]/255;
orange=[255 165 0]/255;
green=[0 205 102]/255;

switch settings.model
    case 'DiM'

        samples=size(y_sim,1);

        figure;
        title('MCA Tracking of perceived signals');

        subplot(3,2,1);
        plot(y_sim(:,1),'r');
        hold on;
        plot(REF(1,1:samples),'k');
        title('Longitudinal: $\hat{a}_x$','Interpreter','latex');

        subplot(3,2,2);
        plot(y_sim(:,2),'r');
        hold on;
        plot(REF(2,1:samples),'k');
        title('Lateral: $\hat{a}_y$','Interpreter','latex');

        subplot(3,2,3);
        plot(y_sim(:,3),'r');
        hold on;
        plot(REF(3,1:samples),'k');
        title('Vertical: $\hat{a}_z$','Interpreter','latex');

        subplot(3,2,4);
        plot(y_sim(:,4),'r');
        hold on;
        plot(REF(4,1:samples),'k');
        title('Roll: $\hat{\omega}_{\psi}$','Interpreter','latex');

        subplot(3,2,5);
        plot(y_sim(:,5),'r');
        hold on;
        plot(REF(5,1:samples),'k');
        title('Pitch: $\hat{\omega}_{\theta}$','Interpreter','latex');

        subplot(3,2,6);
        plot(y_sim(:,6),'r');
        hold on;
        plot(REF(6,1:samples),'k');
        title('Yaw: $\hat{\omega}_{\phi}$','Interpreter','latex');


        figure;
        subplot(3,2,1)
        plot(y_sim(:,13));
        hold on;
        plot(y_sim(:,7),'r');
        plot(zeros(Tf*100,1),'k');
        title('Longitudinal displacement')
        lgd=legend('tripod: $p_{x,T}$','hex: $p_{x,H}$','ref: $p_{x,T}$');
        set(lgd,'Interpreter','latex');

        subplot(3,2,2)
        plot(y_sim(:,14));
        hold on;
        plot(y_sim(:,8),'r');
        plot(zeros(Tf*100,1),'k');
        title('Lateral displacement')
        lgd=legend('tripod: $p_{y,T}$','hex: $p_{y,H}$','ref: $p_{y,T}$');
        set(lgd,'Interpreter','latex');

        subplot(3,2,3)
        plot(y_sim(:,9),'r');
        hold on;
        plot(zeros(Tf*100,1),'k');
        title('Vertical displacement: $p_{z,H}$','Interpreter','latex');

        subplot(3,2,4)
        plot(y_sim(:,20));
        hold on;
        plot(y_sim(:,17),'r');
        plot(zeros(Tf*100,1),'k');
        title('Yaw')
        lgd=legend('tripod: $\phi_T$','hex: $\phi_H$','ref');
        set(lgd,'Interpreter','latex');

        subplot(3,2,5)
        plot(y_sim(:,18),'r');
        hold on;
        plot(zeros(Tf*100,1),'k');
        title('Pitch');
        lgd=legend('hexpod: $\theta_H$','ref');
        set(lgd,'Interpreter','latex');

        subplot(3,2,6)
        plot(y_sim(:,19),'r');
        hold on;
        plot(zeros(Tf*100,1),'k');
        title('Roll');
        lgd=legend('hexpod: $\psi_H$','ref');
        set(lgd,'Interpreter','latex');

        figure;
        title('Hex actuator constraints')
        plot(constraints(:,1:6));
        hold on;
        plot(1.045*ones(samples,1),':');
        plot(1.375*ones(samples,1),':');
        axis([0 iter 1.0 1.4]);
        title('Hexpod actuator constraints');

        % figure;
        % plot(constraints(:,7:9));
        % hold on;
        % plot(2.5*ones(samples,1),':');
        % plot(4*ones(samples,1),':');
        % title('Tripod actuator constraints');

        % figure;
        % subplot(3,2,1)
        % plot(Acc_sim(:,1));
        % title('Longitudinal accelerations')
        % lgd=legend('hexpod: $a_{x,H}$');
        % set(lgd,'Interpreter','latex');
        % 
        % subplot(3,2,2)
        % plot(Acc_sim(:,2));
        % title('Lateral accelerations')
        % lgd=legend('hexpod: $a_{y,H}$');
        % set(lgd,'Interpreter','latex');
        % 
        % subplot(3,2,3)
        % plot(Acc_sim(:,3));
        % title('Vertical accelerations')
        % lgd=legend('hexpod: $a_{z,H}$');
        % set(lgd,'Interpreter','latex');
        % 
        % subplot(3,2,4)
        % plot(Acc_sim(:,4));
        % title('Yaw angle')
        % lgd=legend('hexpod: $\phi_{H}$');
        % set(lgd,'Interpreter','latex');
        % 
        % subplot(3,2,5)
        % plot(Acc_sim(:,1));
        % title('Pitch angle')
        % lgd=legend('hexpod: $\theta_{H}$');
        % set(lgd,'Interpreter','latex');
        % 
        % subplot(3,2,6)
        % plot(Acc_sim(:,1));
        % title('Roll angle')
        % lgd=legend('hexpod: $\psi_{H}$');
        % set(lgd,'Interpreter','latex');

    case 'InvertedPendulum'
        
        figure(1);
        subplot(321)
        plot(time,state_sim(:,1));
        title('p');
        subplot(322)
        plot(time,state_sim(:,2)*180/pi);
        title('\theta');
        subplot(323)
        plot(time,state_sim(:,3));
        title('v');
        subplot(324)
        plot(time,state_sim(:,4)*180/pi);
        title('\omega');
        subplot(3,2,[5 6]);
        title('F');
        stairs(time,controls_MPC(:,1));
        
    case 'ChainofMasses_Lin'
        figure(1);
        subplot(311);
        plot(time,controls_MPC(:,1));
        ylabel('$u_x$','Interpreter','latex');
        subplot(312);
        plot(time,controls_MPC(:,2));
        ylabel('$u_y$','Interpreter','latex');
        subplot(313);
        plot(time,controls_MPC(:,3));
        ylabel('$u_z$','Interpreter','latex');

        figure(2);
        plot3([0,state_sim(1,1:n)], [0,state_sim(1,n+1:2*n)], [0,state_sim(1,2*n+1:3*n)],'Color',red,'LineStyle','--');
        hold on;
        grid on;
        plot3([0,state_sim(end,1:n)], [0,state_sim(end,n+1:2*n)],[0,state_sim(end,2*n+1:3*n)],'Color',blue,'LineStyle','-');
        scatter3([0,state_sim(1,1:n)], [0,state_sim(1,n+1:2*n)], [0,state_sim(1,2*n+1:3*n)],10,'MarkerFaceColor','none');
        scatter3([0,state_sim(end,1:n)], [0,state_sim(end,n+1:2*n)],[0,state_sim(end,2*n+1:3*n)],10,'MarkerFaceColor',red);
        xlabel('X[m]');
        ylabel('Y[m]');
        zlabel('Z[m]');
        
        xlim([-0.5 8]);
        ylim([-1 1]);
        zlim([-6 0.5]);
        
        case 'ChainofMasses_NLin'
        figure(1);
        subplot(311);
        plot(time,controls_MPC(:,1));
        ylabel('$u_x$','Interpreter','latex');
        subplot(312);
        plot(time,controls_MPC(:,2));
        ylabel('$u_y$','Interpreter','latex');
        subplot(313);
        plot(time,controls_MPC(:,3));
        ylabel('$u_z$','Interpreter','latex');

        figure(2);
        plot3([0,state_sim(1,1:n)], [0,state_sim(1,n+1:2*n)], [0,state_sim(1,2*n+1:3*n)],'Color',red,'LineStyle','--');
        hold on;
        grid on;
        plot3([0,state_sim(end,1:n)], [0,state_sim(end,n+1:2*n)],[0,state_sim(end,2*n+1:3*n)],'Color',blue,'LineStyle','-');
        scatter3([0,state_sim(1,1:n)], [0,state_sim(1,n+1:2*n)], [0,state_sim(1,2*n+1:3*n)],10,'MarkerFaceColor','none');
        scatter3([0,state_sim(end,1:n)], [0,state_sim(end,n+1:2*n)],[0,state_sim(end,2*n+1:3*n)],10,'MarkerFaceColor',red);
        xlabel('X[m]');
        ylabel('Y[m]');
        zlabel('Z[m]');
        
%         xlim([-0.5 8]);
%         ylim([-1 1]);
%         zlim([-6 0.5]);
end

