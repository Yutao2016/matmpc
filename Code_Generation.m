clear all;clc;
display('-----------------------------------------------------');
display('This framework is developed by Yutao Chen, DEI, UniPD');
display('-----------------------------------------------------');


%% Insert Model here
settings.model='InvertedPendulum';

switch settings.model
    case 'InvertedPendulum'
        InvertedPendulum;
    case 'ChainofMasses_Lin'
        ChainofMasses_Lin;
end

%% Integrator configurations

input.opt.integrator='ERK4'; % 'ERK4', 'IRK4'

%%
import casadi.*

z = [states;controls];

lambdai=SX.sym('lambdai',nx,1);            % the i th multiplier for equality constraints
lambda_next=SX.sym('lambda_next',nx,1);    % the i+1 th multiplier for equality constraints
mui=SX.sym('mui',2*nc,1);                  % the i th multiplier for inequality constraints
muN=SX.sym('muN',2*ncN,1);                 % the N th multiplier for inequality constraints

Ts    = 0.05;  % NMPC sampling time [s]

%% Explicit Runge-Kutta 4 Integrator for simulation
s  = 10; % No. of integration steps per sample interval
DT = Ts/s;
f  = Function('f', {states,controls,params}, {x_dot},{'states','controls','params'},{'xdot'});
X=states;
U=controls; 
P=params;
for j=1:s
       [k1] = f(X, U, P);
       [k2] = f(X + DT/2 * k1, U, P);
       [k3] = f(X + DT/2 * k2, U, P);
       [k4] = f(X + DT * k3, U, P);
       X=X+DT/6*(k1 +2*k2 +2*k3 +k4);
end
Simulate_system = Function('Simulate_system', {states,controls,params}, {X}, {'states','controls','params'}, {'xf'});

%% Explicit Runge-Kutta 4 Integrator for multiple shooting

if strcmp(input.opt.integrator, 'ERK4')

    s  = 5; % No. of integration steps per shooting interval
    DT = Ts/s;
    f  = Function('f', {states,controls,params}, {x_dot},{'states','controls','params'},{'xdot'});
    X=states;
    U=controls; 
    P=params;
    for j=1:s
           [k1] = f(X, U, P);
           [k2] = f(X + DT/2 * k1, U, P);
           [k3] = f(X + DT/2 * k2, U, P);
           [k4] = f(X + DT * k3, U, P);
           X=X+DT/6*(k1 +2*k2 +2*k3 +k4);
    end
    F = Function('F', {z,params}, {X}, {'z','params'}, {'xf'});
    D= F.jacobian('z','xf');

end

%% Implicit Runge-Kutta 4 Integrator
if strcmp(input.opt.integrator, 'IRK4')
    s=3; % No. of collocation points

    DT=SX.sym('DT',1,1); % Integration stage length

    A=[5/36,             2/9-sqrt(15)/15, 5/36-sqrt(15)/30;...
       5/36+sqrt(15)/24, 2/9            , 5/36-sqrt(15)/24;...
       5/36+sqrt(15)/30, 2/9+sqrt(15)/15, 5/36];
    B=[5/18;4/9;5/18]; % Gauss-Legendre collocation matrix

    k=SX.sym('k',s*nx,1);     % Intermediate state

    X_irk=states;
    U_irk=controls; 
    P_irk=params;
    x_end=zeros(nx,1);
    for i=1:s
        x_end= x_end+ DT*B(i)*k((i-1)*nx+1:i*nx,1);
    end
    x_end=X_irk+x_end;
    phi=Function('phi',{k,X_irk,DT},{x_end},{'k','X_irk','DT'},{'x_end'});
    dphi_dk=phi.jacobian('k','x_end');

    nonlinear_system=[];
    for i=1:s
        rhs=zeros(nx,1);
        for j=1:s
            rhs=rhs+DT*A(i,j)*k((j-1)*nx+1:j*nx,1);
        end
        nonlinear_system=[nonlinear_system;...
                          k((i-1)*nx+1:i*nx,1)-f(X_irk+rhs,U_irk,P_irk);...
                          ];
    end
    w_irk=[X_irk;U_irk];

    F_NS=Function('F_NS',{k,w_irk,P_irk,DT},{nonlinear_system},{'k','w_irk','P_irk','DT'},{'NS'});
    dF_dk=F_NS.jacobian('k','NS');
    dF_dw=F_NS.jacobian('w_irk','NS');

    lambda_k=SX.sym('lambda_k',s*nx,1);
    adj_F=nonlinear_system'*lambda_k;

    dF_dk_adj=Function('dF_dk_adj',{k,w_irk,P_irk,DT,lambda_k},{jacobian(adj_F,k)'},{'k','w_irk','P_irk','DT','lambda_k'},{'dF_dk_adj'});
    dF_dw_adj=Function('dF_dw_adj',{k,w_irk,P_irk,DT,lambda_k},{jacobian(adj_F,w_irk)'},{'k','w_irk','P_irk','DT','lambda_k'},{'dF_dw_adj'});

    % adjoint hessian
    d2F_d2k=Function('d2F_d2k',{k,w_irk,P_irk,DT,lambda_k},{hessian(adj_F,k)},{'k','w_irk','P_irk','DT','lambda_k'},{'d2F_d2k'});
    d2F_d2w=Function('d2F_d2w',{k,w_irk,P_irk,DT,lambda_k},{hessian(adj_F,w_irk)},{'k','w_irk','P_irk','DT','lambda_k'},{'d2F_d2w'});
    d2F_dk_dw=dF_dk_adj.jacobian('w_irk','dF_dk_adj');
    d2F_dw_dk=dF_dw_adj.jacobian('k','dF_dw_adj');
end

%% objective and constraints

obji_vec=sqrt(Q)*(h_fun(states,controls,params)-refs);
objN_vec=sqrt(QN)*(hN_fun(states,params)-refN);

obji=0.5*norm_2(obji_vec)^2;
objN=0.5*norm_2(objN_vec)^2;

obji_vec_fun=Function('obji_vec_fun',{z,refs,params,Q},{obji_vec},{'z','refs','params','Q'},{'obji_vec'});
objN_vec_fun=Function('objN_vec_fun',{states,refN,params,QN},{objN_vec},{'states','refN','params','QN'},{'objN_vec'});
dobji_vec_fun=obji_vec_fun.jacobian('z','obji_vec');
dobjN_vec_fun=objN_vec_fun.jacobian('states','objN_vec');

dJdz=Function('dJdz',{z,refs,params,Q},{jacobian(obji,z)'},{'z','refs','params','Q'},{'dJdz'});
dJNdz=Function('dJNdz',{states,refN,params,QN},{jacobian(objN,states)'},{'states','refN','params','QN'},{'dJNdz'});

dBdz=Function('dBdz',{z},{jacobian([ineq;-ineq],z)},{'z'},{'dBdz'});
dBNdz=Function('dBNdz',{states},{jacobian([ineqN;-ineqN],states)},{'states'},{'dBNdz'});

dJ2dz=hessian(obji,z);
dB2dz=hessian([ineq;-ineq]'*mui,z);
dJN2dz=hessian(objN,states);
dBN2dz=hessian([ineqN;-ineqN]'*muN,states);

dJ2dz_fun=Function('dJ2dz_fun',{z,refs,params,Q},{dJ2dz},{'z','refs','params','Q'},{'dJ2dz'});
dB2dz_fun=Function('dB2dz_fun',{z,mui},{dB2dz},{'z','mui'},{'dB2dz'});
dJN2dz_fun=Function('dJN2dz_fun',{states,refN,params,QN},{dJN2dz},{'states','refN','params','QN'},{'dJN2dz'});
dBN2dz_fun=Function('dBN2dz_fun',{states,muN},{dBN2dz},{'states','muN'},{'dBN2dz'});

if strcmp(input.opt.integrator, 'ERK4')
    adj_dG=jacobian(X'*lambda_next,z)'-[lambdai;SX(nu,1)];
    adj_dB=jacobian([ineq;-ineq]'*mui,z)';
    adj_dBN=jacobian([ineqN;-ineqN]'*muN,states)';

    dLi=jacobian(obji,z)'+adj_dG+adj_dB;
    dLi_fun=Function('dLi_fun',{z,refs,params,Q,lambda_next,lambdai,mui},{dLi},{'z','refs','params','Q','lambda_next','lambdai','mui'},{'dLi'});
    dLN=jacobian(objN,states)'-lambdai+adj_dBN;
    dLN_fun=Function('dLN_fun',{states,refN,params,QN,lambdai,muN},{dLN},{'states','refN','params','Q','lambdai','muN'},{'dLN'});
    
    dG2dz=hessian(X'*lambda_next,z);    
    Hi_EX=Function('Hi_EX',{z,refs,params,Q,lambda_next,mui},{dJ2dz+dG2dz+dB2dz},{'z','refs','params','Q','lambda_next','mui'},{'Hi'});
    HN_EX=Function('HN_EX',{states,refN,params,QN,muN},{dJN2dz+dBN2dz},{'states','refN','params','QN','muN'},{'HN'});  
end


%% Code generation and Compile

generate=builtin('input','Would you like to generate the source code?(y/n)','s');


if strcmp(generate,'y')

    display('                           ');
    display('Generating source code...');

    if exist('Source_Codes','dir')~=7
        mkdir('Source_Codes');
    end

    cd Source_Codes

    opts = struct( 'main', false, 'mex' , true ) ;
    
    Simulate_system.generate('Simulate_system.c',opts);
    
    h_fun.generate('h_fun.c',opts);
    ineq_fun.generate('ineq_fun.c',opts);
    ineqN_fun.generate('ineqN_fun.c',opts);
    
    obji_vec_fun.generate('obji_vec_fun.c',opts);
    objN_vec_fun.generate('objN_vec_fun.c',opts);
    dobji_vec_fun.generate('dobji_vec_fun.c',opts);
    dobjN_vec_fun.generate('dobjN_vec_fun.c',opts);
    dJdz.generate('dJdz.c',opts);
    dJNdz.generate('dJNdz.c',opts);
    dBdz.generate('dBdz.c',opts);
    dBNdz.generate('dBNdz.c',opts);
    dJ2dz_fun.generate('dJ2dz_fun.c',opts);
    dB2dz_fun.generate('dB2dz_fun.c',opts);
    dJN2dz_fun.generate('dJN2dz_fun.c',opts);
    dBN2dz_fun.generate('dBN2dz_fun.c',opts);
    
    if strcmp(input.opt.integrator, 'ERK4')
        F.generate('F.c',opts);
        D.generate('D.c',opts);
        dLi_fun.generate('dLi_fun.c',opts);
        dLN_fun.generate('dLN_fun.c',opts);        
        Hi_EX.generate('Hi_EX.c',opts);
        HN_EX.generate('HN_EX.c',opts);
    end
              
    if strcmp(input.opt.integrator, 'IRK4')
        cd ..
        cd IRK
            F_NS.generate('F_NS.c',opts);
            dF_dk.generate('dF_dk.c',opts);
            dF_dw.generate('dF_dw.c',opts);
            phi.generate('phi.c',opts);
            dphi_dk.generate('dphi_dk.c',opts);
            dF_dw_adj.generate('dF_dw_adj.c',opts);
            dF_dk_adj.generate('dF_dk_adj.c',opts);
            d2F_d2k.generate('d2F_d2k.c',opts);
            d2F_d2w.generate('d2F_d2w.c',opts);
            d2F_dk_dw.generate('d2F_dk_dw.c',opts);
            d2F_dw_dk.generate('d2F_dw_dk.c',opts);                    
        cd ..
        cd Source_Codes
    end
    

display('                           ');
display('Code generation completed!');

end

compile=builtin('input','Would you like to compile the source code?(y/n)','s');
if strcmp(compile,'y')
    
    display('                           ');
    display('Compiling...(turn off optimization flag is recommended)');
    op_flag=builtin('input','Would you like to turn OFF the optimization flag?(y/n)','s');

    if isempty(op_flag)
        op_flag = 'y';
    end

    if strcmp(op_flag,'n')
        display('Optimization flag turned on');
        display('                           ');
                
        mex -largeArrayDims ineq_fun.c
        mex -largeArrayDims ineqN_fun.c
        mex -largeArrayDims h_fun.c
        mex -largeArrayDims Simulate_system.c
        
        mex -largeArrayDims obji_vec_fun.c
        mex -largeArrayDims objN_vec_fun.c
        mex -largeArrayDims dobji_vec_fun.c
        mex -largeArrayDims dobjN_vec_fun.c
        mex -largeArrayDims dBdz.c
        mex -largeArrayDims dBNdz.c
        mex -largeArrayDims dJdz.c        
        mex -largeArrayDims dJNdz.c
        mex -largeArrayDims dJ2dz_fun.c
        mex -largeArrayDims dB2dz_fun.c
        mex -largeArrayDims dJN2dz_fun.c
        mex -largeArrayDims dBN2dz_fun.c
        
        if strcmp(input.opt.integrator, 'ERK4')
            mex -largeArrayDims F.c 
            mex -largeArrayDims D.c 
            mex -largeArrayDims dLi_fun.c
            mex -largeArrayDims dLN_fun.c 
            mex -largeArrayDims Hi_EX.c
            mex -largeArrayDims HN_EX.c
        end

        if strcmp(input.opt.integrator, 'IRK4')
            cd ..
            cd IRK
                mex -largeArrayDims F_NS.c 
                mex -largeArrayDims dF_dk.c 
                mex -largeArrayDims dF_dw.c 
                mex -largeArrayDims phi.c 
                mex -largeArrayDims dphi_dk.c
                mex -largeArrayDims dF_dw_adj.c
                mex -largeArrayDims dF_dk_adj.c
                mex -largeArrayDims d2F_d2k.c
                mex -largeArrayDims d2F_d2w.c
                mex -largeArrayDims d2F_dk_dw.c
                mex -largeArrayDims d2F_dw_dk.c                          
            cd ..
            cd Source_Codes
        end
    end

    if strcmp(op_flag,'y')
        display('Optimization flag turned off');
        display('                            ');
        
        mex -largeArrayDims -g ineq_fun.c
        mex -largeArrayDims -g ineqN_fun.c
        mex -largeArrayDims -g h_fun.c
        mex -largeArrayDims -g Simulate_system.c
        
        mex -largeArrayDims -g obji_vec_fun.c
        mex -largeArrayDims -g objN_vec_fun.c
        mex -largeArrayDims -g dobji_vec_fun.c
        mex -largeArrayDims -g dobjN_vec_fun.c
        mex -largeArrayDims -g dBdz.c
        mex -largeArrayDims -g dBNdz.c
        mex -largeArrayDims -g dJdz.c        
        mex -largeArrayDims -g dJNdz.c
        
        if strcmp(input.opt.integrator, 'ERK4')
            mex -largeArrayDims -g F.c 
            mex -largeArrayDims -g D.c 
            mex -largeArrayDims -g dLi_fun.c
            mex -largeArrayDims -g dLN_fun.c
            mex -largeArrayDims -g dJ2dz_fun.c
            mex -largeArrayDims -g dB2dz_fun.c
            mex -largeArrayDims -g dJN2dz_fun.c
            mex -largeArrayDims -g dBN2dz_fun.c
            mex -largeArrayDims -g Hi_EX.c
            mex -largeArrayDims -g HN_EX.c
        end
           
        if strcmp(input.opt.integrator, 'IRK4')
            cd ..
            cd IRK
                mex -largeArrayDims -g F_NS.c 
                mex -largeArrayDims -g dF_dk.c 
                mex -largeArrayDims -g dF_dw.c 
                mex -largeArrayDims -g phi.c 
                mex -largeArrayDims -g dphi_dk.c 
                mex -largeArrayDims -g dF_dw_adj.c
                mex -largeArrayDims -g dF_dk_adj.c
                mex -largeArrayDims -g d2F_d2k.c
                mex -largeArrayDims -g d2F_d2w.c
                mex -largeArrayDims -g d2F_dk_dw.c
                mex -largeArrayDims -g d2F_dw_dk.c
                delete('*.pdb');
            cd ..
            cd Source_Codes
        end
        delete('*.pdb');
    end

cd ..
display('                           ');
display('Compilation completed!');

end
%% NMPC preparation

display('                           ');
display('Preparing the NMPC solver...');

settings.Ts = Ts;  
settings.nx = nx; 
settings.nu = nu;    
settings.ny = ny;    
settings.nyN= nyN;    
settings.np = np;   
settings.nc = nc;
settings.ncN = ncN;

save('settings','settings');
save('input','input');

clear all;

display('                           ');
display('NMPC solver prepared! Enjoy solving...');
display('                           ');
