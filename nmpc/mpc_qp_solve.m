function [solution,lambda,mu,fval,info,opt] = mpc_qp_solve(sizes,H,g,dB,B,opt,G)

    nu=sizes.nu;
    nineq=sizes.nineq;    
    N=sizes.N;   

    switch(opt.qpsolver)
        case 'ipopt'
            H=sparse(H);
            opt.ipopt.funcs.hessian = @(x,sigma,lambda) tril(sigma*H);
            opt.ipopt.funcs.hessianstructure = @() tril(H);
            opt.ipopt.funcs.objective=@(x) 0.5*x'*H*x + g'*x;
            opt.ipopt.funcs.gradient = @(x) (H*x + g)';
            opt.ipopt.options.A=sparse(dB);
            opt.ipopt.options.ru=-B;
            opt.ipopt.options.rl=[-Inf*ones(nineq,1);-G];
            [solution,fval,exitflag,info_ipopt] = opti_ipopt(opt.ipopt,[]);
            lambda   = info_ipopt.Lambda.eqlin;
            mu       = info_ipopt.Lambda.ineqlin;
            info.cpt_qp   = info_ipopt.Time*1e3;
            info.exitflag = exitflag;
                        
        case 'qpoases'
            if opt.condensing_matrix.QP==0 
                [QP,solution,fval,exitflag,iterations,multiplier,auxOutput] = qpOASES_sequence('i',H,g,dB,[],[],[],-B); 
                opt.condensing_matrix.QP=QP;
            else
                QP=opt.condensing_matrix.QP;
                if strcmp(opt.hotstart,'no')
                    [solution,fval,exitflag,iterations,multiplier,auxOutput] = qpOASES_sequence('m',QP,H,g,dB,[],[],[],-B);
                end
                if strcmp(opt.hotstart,'yes')
                    [solution,fval,exitflag,iterations,multiplier,auxOutput] = qpOASES_sequence('h',QP,g,[],[],[],-B);
                end
            end           
            mu   = - multiplier(N*nu+1:end);
            lambda = 0;
            info.cpt_qp   = auxOutput.cpuTime*1e3;
            info.exitflag = exitflag;           
    end
end
