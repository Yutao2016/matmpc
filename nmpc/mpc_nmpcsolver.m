function [output] = mpc_nmpcsolver(input,settings,mpc_callnum,kkt_lim)
    tic;
    x0 = input.x0;
    z  = input.z;
    xN  = input.xN;
    y  = input.y;
    yN = input.yN;
    od = input.od;       
    lambda = input.lambda;
    mu = input.mu;
    muN = input.muN;
    Q = input.W;
    QN = input.WN;
       
    N=settings.N;
    nx=settings.nx;
    nu=settings.nu;
    nc=settings.nc;
    ncN=settings.ncN;
    nw=settings.nw;

    i=0;
    Termination = 1e8;
    
    CPT.INT=0;
    CPT.SENS=0;
    CPT.COND=0;
    CPT.QP=0;
   
    while(i < mpc_callnum  &&  Termination>kkt_lim) % RTI or multiple call
        %% ----------- QP Preparation

        [H,g_vec,G,dG,dB,B_vec,tINT,tSENS,input] = mpc_qp_generation(z,xN,y,yN,od,lambda,mu,muN,Q,QN,input,settings);        
       
        % Transform the QP data to matrices and vectors      
        H_mat=blkdiag(H{:});
        
        if ~strcmp(input.opt.condensing,'standard')
            dG_mat=[sparse(nx,nw);blkdiag(dG{:}),sparse(N*nx,nx)]+input.data.I_dG;
            G_vec=[input.x0-z(1:nx,1); G];
        else
            dG_mat=dG;
            G_vec=G;
        end
        dB_mat=blkdiag(dB{:});
        
        %% ---------- Solve the QP              
        if strcmp(input.opt.condensing,'no')
            if strcmp( input.opt.qpsolver,'qpoases')
                error('Using qpOASES without condensing is not encouraging');
            else                            
                [dw,lambda_vec,mu_vec,~,info,input.opt] = mpc_qp_solve(settings,H_mat,g_vec,[dB_mat;dG_mat],[B_vec;G_vec],input.opt,G_vec);
                info.tcond=0;   
                lambda = reshape(lambda_vec,[nx N+1]);
            end
        else
            [dw,lambda,mu_vec,info,input.opt] = mpc_qp_condense_and_solve(settings,x0,H_mat,g_vec,G_vec,dG_mat,B_vec,dB_mat,z,input);           
        end
        
        % reshape the solution
        dz=reshape(dw(1:N*(nx+nu)), [nx+nu N]);
        dxN=dw(N*(nx+nu)+1:end,1);       
        mu=reshape(mu_vec(1:2*N*nc), [2*nc N]);
        muN=mu_vec(2*N*nc+1:2*N*nc+2*ncN);
        
        %% ---------- Line search
        [z,xN,lambda,mu,muN, mu_merit,alpha] = mpc_line_search(z,xN,dz,dxN,dw,od,y,yN,g_vec,H_mat,G_vec,dG_mat,B_vec,dB_mat,Q,QN,lambda,mu,muN,input,settings,mpc_callnum);
        
        %% ---------- KKT calculation 
        [objVal,Termination,eq_res,ineq_res] = mpc_get_objfun_solution(z,xN,y,yN,od,lambda,mu,muN,input,settings);
        
        %% ---------- Multiple call management and convergence check
                        
        CPT.INT=CPT.INT+tINT;
        CPT.SENS=CPT.SENS+tSENS;
        CPT.COND=CPT.COND+info.tcond;
        CPT.QP=CPT.QP+info.cpt_qp;
               
        input.opt.meritfun.mu_merit = mu_merit;
        
        i=i+1;
    end

    output.info.cpuTime=toc*1e3;   % Total CPU time for the current sampling instant
    
    output.z=z;
    output.xN=xN;   
    output.lambda=lambda;
    output.mu=mu;
    output.muN=muN;

    output.info.alpha=alpha;
    output.info.kktValue=Termination;
    output.info.objValue=objVal;
    output.info.eq_res=eq_res;
    output.info.ineq_res=ineq_res;
    output.info.iteration_num=i;    
    output.info.intTime=CPT.INT;
    output.info.sensTime=CPT.SENS;
    output.info.condTime=CPT.COND;
    output.info.qpTime=CPT.QP;
    output.info.status=info.exitflag;
    
    if strcmp(input.opt.qpsolver,'qpoases')
        output.opt.condensing_matrix.QP=input.opt.condensing_matrix.QP;
    end

    output.meritfun=input.opt.meritfun;
end

