
function [objVal,Termination,eq_res,ineq_res] = mpc_get_objfun_solution(z,xN,y,yN,od,lambda,mu,muN,input,sizes) 
    
    
    Q = input.W;
    QN = input.WN;

    N=sizes.N;
    nx=sizes.nx;
    nu=sizes.nu;
    nc=sizes.nc;
    ny=sizes.ny;
    nz = nx+nu;
       
    obj_vec=input.data.obj_vec;
    G=input.data.G; 
    B=input.data.B;    
    dL=input.data.dL; 
    
    x_next=[z(1:nx,2:end),xN];
    lambda_next=lambda(:,2:end);
    
    lb=input.lb;
    ub=input.ub;
    lbN=input.lbN;
    ubN=input.ubN;
    
    integrator=input.opt.integrator; 
            
    for i=1:N
           
        zi = z(:,i);
        refi = y(:,i);
        if ~isempty(od)
            parai = od(:,i);
        else
            parai = 0;
        end
        
        lambdai = lambda(:,i);              
        mui = mu(:,i);
        
        obj_vec(:,i)=full(obji_vec_fun('obji_vec_fun',zi,refi,parai,Q));
              
        switch integrator
            case 'ERK4'
                G(:,i)=full(F('F',zi,parai))-x_next(:,i);
            case 'IRK4'
                [ xk_end, ~, sens_info] = IRK_INT_M(sizes, zi(1:nx), zi(nx+1:nx+nu), parai );
                G(:,i)=xk_end-x_next(:,i);
        end
        
        bi=full(ineq_fun('ineq_fun',zi(1:nx), zi(nx+1:nx+nu), parai));
        B(:,i)=[bi-ub(:,i);lb(:,i)-bi];
        
        switch integrator
            case 'ERK4'
                dL(:,i)=full(dLi_fun('dLi_fun',zi,refi,parai,Q,lambda_next(:,i),lambdai,mui));
            case 'IRK4'                                      
                [ ADJ, ~] = IRK_ADJ_SENS_M(sizes, sens_info, lambda_next(:,i),lambdai );
                dL(:,i)= dJdz('dJdz',zi,refi,parai,Q)...
                       +ADJ...
                       +dBdz('dBdz',zi)'*mui;                                                                                          
        end
    end
    
    i=N+1;
    zN = xN;
    lambdaN = lambda(:,i);
  
    refN = yN;
    if ~isempty(od)
         paraN = od(:,i);
     else
         paraN = 0;
    end
        
    obj_vec_N=full(objN_vec_fun('objN_vec_fun',zN,refN,paraN,QN));
    
    bN=full(ineqN_fun('ineqN_fun',zN,paraN));
    BN=[bN-ubN;lbN-bN];
    
    switch integrator
            case 'ERK4'
                dLN=full(dLN_fun('dLN_fun',zN,refN,paraN,QN,lambdaN,muN));
            case 'IRK4'
                dLN= dJNdz('dJNdz',zN,refN,paraN,QN)...
                       -lambdaN...
                       +dBNdz('dBNdz',zN)'*muN;
    end
    %%
    
    G=[input.x0-z(1:nx,1); reshape(G,[N*nx,1])];
    B = [reshape(B,[N*2*nc,1]);BN];
    obj_vec = [reshape(obj_vec,[N*ny,1]);obj_vec_N];
    dL = [reshape(dL,[N*nz,1]);dLN];
    
    eq_res=norm( G,1 );
    ineq_res=sum( max( B,0 ) );
    KKT=norm( dL,2 ); 
    
    Termination=max([KKT, eq_res, ineq_res]);
        
    objVal = 0.5*norm(obj_vec,2)^2;
    
end